using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using DearVR;

public class DearVRScriptLoad : MonoBehaviour {

	enum ReverbType {
		internalReverb,
		mixerReverb
	}

	[SerializeField] ReverbType reverbType = ReverbType.internalReverb;

	AudioSource myAudioSource;

	DearVRSource myDearVRSource;

	// Assign in Inspector or in script
	[SerializeField] AudioClip myAudioClip;

	[SerializeField] DearVRSource.RoomList roomSelection;

	[SerializeField] DearVRSerializedReverb[] reverbSendList;

	[SerializeField] AudioMixerGroup audioMixer;

	[SerializeField] bool performanceMode = false;
	
	[SerializeField] bool loop = false;

	void Awake () {

		// Create dearVR-Instance
		myDearVRSource = gameObject.AddComponent<DearVRSource>();

		myDearVRSource.PerformanceMode = performanceMode;

		// Assign and set AudioSource
		myAudioSource = myDearVRSource.audioSource;

		myAudioSource.loop = loop;
		
		if (performanceMode) {
			myAudioSource.playOnAwake = false;
		}
		// Select Room Preset
		myDearVRSource.RoomPreset = roomSelection;

		// Select reverb type, between internal reverb or sending the reverb to mixer bus
		switch(reverbType) {

		// dearVR Source Internal Reverb Part
		case ReverbType.internalReverb:
		
			myDearVRSource.InternalReverb = true;

			break;
		
			// dearVR Reverb Part
		case ReverbType.mixerReverb: 

			myDearVRSource.InternalReverb = false;

			myAudioSource.outputAudioMixerGroup = audioMixer;

			if (reverbSendList != null && reverbSendList.GetLength(0) > 0) {
			
				myDearVRSource.SetReverbSends(reverbSendList);
			
			}

			break;
		
		}
			
//		for (int i = 0; i < myDearVRSource.GetReverbSendList().Length; i++)
//		{
//			Debug.LogWarning("myDearVRSource.GetReverbSendList();" + myDearVRSource.GetReverbSendList()[i].send);	
//		}



		// Set dearVR-Settings
		myDearVRSource.BassBoost = false;

		myDearVRSource.InputChannel = 1.0f;


	// Assign AudioClip
	if (myAudioClip) {

		myAudioSource.clip = myAudioClip;

			if (performanceMode) {
//				Debug.Log("DearVRPlayOnAwake" + myDearVRSource.name);
				myDearVRSource.DearVRPlay();

			} else {
//				Debug.Log("PlayOnAwake" + myAudioSource.name);
				myAudioSource.Play ();

			}


			} else {

				Debug.LogWarning("DEARVR: AudioClip not assigned!");

			}

	}


	public void Deactivate()
	{
//		Debug.Log(this.name + " deactivate");
		gameObject.SetActive(false);
	}

	public void Activate()
	{
//		Debug.Log(this.name + " activate");
		gameObject.SetActive(true);
	}

	public void PlayScript()
	{
//		Debug.Log(this.name + " try to play");
		if (myAudioSource)
		{
//			Debug.Log(this.name + " play");
			myAudioSource.Play();   
		}
	}

	
}
