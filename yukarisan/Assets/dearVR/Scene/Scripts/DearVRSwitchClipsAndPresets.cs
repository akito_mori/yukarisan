﻿using UnityEngine;
using System.Collections;
using DearVR;

public class DearVRSwitchClipsAndPresets : MonoBehaviour {


	[SerializeField] AudioClip [] PlayClip;

	[SerializeField] int ClipIndex = 0;

	[SerializeField] int RoomIndex = 4;

	AudioSource Source;

	DearVRSource dearVRInstance;


	void Start () {
		
		Debug.Log("DEARVR Demo Scene: press F or G to switch AudioClips and press R or T to switch RoomPresets");

		Source = GetComponent<AudioSource>();

		dearVRInstance = GetComponent<DearVRSource>();

		Source.clip = PlayClip[0];

		RoomIndex = (int)dearVRInstance.RoomPreset;

		Source.Play();

	}


	void Update () {
	
		if(Input.GetKeyDown(KeyCode.G)) {
			
			ClipIndex = (ClipIndex + 1)%PlayClip.Length;

			Source.clip = PlayClip[ClipIndex];

			Source.Play();

		}

		if(Input.GetKeyDown(KeyCode.F)) {
			
			ClipIndex--;

			if (ClipIndex < 0) {
			
				ClipIndex = PlayClip.Length - 1;

			}

			Source.clip = PlayClip[ClipIndex];

			Source.Play();

		}


		if(Input.GetKeyDown((KeyCode.T))) {

			if (RoomIndex >= (int)DearVRSource.RoomList.String_Plate) {
				
				RoomIndex = 0;

			} else {
			
				RoomIndex++;
			
			}

			dearVRInstance.RoomPreset = (DearVRSource.RoomList)(RoomIndex);
		}

		if(Input.GetKeyDown((KeyCode.R))) {

			if (RoomIndex <= 0) {

				RoomIndex = (int)DearVRSource.RoomList.String_Plate;
			
			} else {
			
				RoomIndex--;	
			
			}

			dearVRInstance.RoomPreset = (DearVRSource.RoomList)(RoomIndex);
		
		}

	}
		
}
