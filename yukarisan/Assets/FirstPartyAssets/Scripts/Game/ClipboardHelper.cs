﻿using UnityEngine;


namespace HC
{
    public class ClipboardHelper
    {
        public static string clipBoard
        {
            get { return GUIUtility.systemCopyBuffer; }
            set { GUIUtility.systemCopyBuffer = value; }
        }
    }
}