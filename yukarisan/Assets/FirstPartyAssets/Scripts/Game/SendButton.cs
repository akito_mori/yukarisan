﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using MiniJSON;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace HC
{
    [DisallowMultipleComponent]
    sealed public class SendButton : MonoBehaviourBase
    {
        #region variable
        [SerializeField]
        private Text m_answer = null;
        [SerializeField]
        private float m_timeOut = 10f;

        /// <summary>
        /// 雑談APIのURL
        /// </summary>
        private string m_reqUrl = "https://api.apigw.smt.docomo.ne.jp/dialogue/v1/dialogue";

        /// <summary>
        /// 雑談APIのAPIKey
        /// </summary>
        private string m_apiKey = "553277336b682f3371303546613677367a38736b6667364c4d5a6e68553233485471512e745362534c472f";

        private WWW m_responsWWW = null;
        private byte[] m_userWordByte = null;
        private UserInfo m_user = new UserInfo();
        private Dictionary<string, string> m_httpHeader = new Dictionary<string, string>() { { "Content-Type", "application/json; charset=UTF-8" } };

        [SerializeField]
        private Button _button = null;
        [SerializeField]
        private InputField _inputField = null;
        #endregion

        #region event
        private void Start()
        {
            _button.onClick.AsObservable()
                .Subscribe(_ => Chat(_inputField.text));
        }
        #endregion

        #region method
        /// <summary>
        /// 雑談APIと雑談する
        /// </summary>
        private void Chat(string message)
        {
            if (message != null)
            {
                // ignore new line
                m_user.Utt = message.Replace("\r", "").Replace("\n", "");
                string userWord = m_user.ExportJson();
                m_userWordByte = Encoding.UTF8.GetBytes(userWord);
                StartCoroutine(HTTPRequest());
            }
        }

        /// <summary>
        /// HTTPリクエストをする
        /// </summary>
        /// <returns></returns>
        private IEnumerator HTTPRequest()
        {
            using (m_responsWWW = new WWW(m_reqUrl + "?APIKEY=" + m_apiKey, m_userWordByte, m_httpHeader))
            {
                yield return StartCoroutine(CheckTimeOut(m_responsWWW));

                if (m_responsWWW != null)
                {
                    var resJson = (IDictionary)Json.Deserialize(m_responsWWW.text);

                    if (m_responsWWW.error != null)
                    {
                        // When error occurred.
                        Debug.Log("error has been occurred");
                        Debug.Log(m_responsWWW.error);
                        var requestError = (IDictionary)resJson["requestError"];
                        var serviceException = (IDictionary)requestError["serviceException"];
                        string errorMessage = (string)serviceException["text"];
                        ChangeMessage("エラーが発生してしまったみたいです。しばらく待ってから再接続してください。\n" + m_responsWWW.error + "\n" + errorMessage);
                    }
                    else
                    {
                        // When answer recieved.
                        m_user.Mode = (string)resJson["mode"];
                        m_user.Context = (string)resJson["context"];
                        string resUtt = (string)resJson["utt"];

                        ChangeMessage(resUtt);
                    }
                }
            }
        }

        /// <summary>
        /// メッセージを加工して変更する
        /// </summary>
        /// <param name="answerMessage">雑談APIの回答</param>
        private void ChangeMessage(string answerMessage)
        {
            m_answer.text = answerMessage.Replace("ゼロ", "結月ゆかり");
            Debug.Log(m_answer.text);
        }

        /// <summary>
        /// タイムアウトするかチェックする
        /// </summary>
        /// <param name="www"></param>
        /// <returns></returns>
        private IEnumerator CheckTimeOut(WWW www)
        {
            float startTime = Time.time;

            while (!www.isDone)
            {
                if (Time.time - startTime < m_timeOut)
                {
                    yield return null;
                }
                else
                {
                    // Timeout.
                    m_responsWWW.Dispose();
                    m_responsWWW = null;
                    ChangeMessage("タイムアウトになってしまったみたいです。インターネット接続を確認してしばらく待ってから再接続してください。");
                    break;
                }
            }

            yield return null;
        }
        #endregion
    }
}