﻿using UnityEngine;


namespace HC
{
    [DisallowMultipleComponent]
    sealed public class ClipBoardTest : MonoBehaviourBase
    {
        int cnt = 0;
        float time = 0f;

        public string[] messages = { "c)こんにちは", "c)結月ゆかり", "c)です" };
        public float delayTime = 3.0f;

        // Update is called once per frame
        void Update()
        {
            time += Time.deltaTime;
            if (time > delayTime)
            {
                time -= delayTime;
                ClipboardHelper.clipBoard = messages[cnt];
                cnt = (cnt + 1) % messages.Length;
            }
        }
    }
}