﻿using UnityEngine;
using System;
using System.Reflection;


namespace HC
{
    sealed public class UserInfo
    {
        #region variable
        /// <summary>
        /// ユーザの発話を指定(255文字以下)
        /// </summary>
        private string utt = null;

        /// <summary>
        /// コンテキストIDを指定(255文字以下)
        /// </summary>
        private string context = null;

        /// <summary>
        /// ユーザのニックネームを指定(全角10文字(半角10文字)以下)
        /// </summary>
        private string nickname = "戸森";

        /// <summary>
        /// ユーザのニックネームの読みを指定(全角20文字以下(カタカナのみ))
        /// </summary>
        private string nickname_y = "トモリ";

        /// <summary>
        /// ユーザの性別は、下記のいずれかを指定
        /// 男、女
        /// </summary>
        private string sex = "男";

        /// <summary>
        /// ユーザの血液型は、下記のいずれかを指定
        /// A、B、AB、O
        /// </summary>
        private string bloodtype = "B";

        /// <summary>
        /// ユーザの誕生日(年)を指定(1～現在までのいずれかの整数(半角4文字以下))
        /// </summary>
        private int birthdateY = 1995;

        /// <summary>
        /// ユーザの誕生日(月)を指定(1～12までのいずれかの整数)
        /// </summary>
        private int birthdateM = 7;

        /// <summary>
        /// ユーザの誕生日(日)を指定(1～31までのいずれかの整数)
        /// </summary>
        private int birthdateD = 26;

        /// <summary>
        /// ユーザの年齢を指定(正の整数(半角3文字以下))
        /// </summary>
        private int age = 21;

        /// <summary>
        /// ユーザの星座は、下記のいずれかを指定
        /// 牡羊座、牡牛座、双子座、蟹座、獅子座、乙女座、天秤座、蠍座、射手座、山羊座、水瓶座、魚座
        /// </summary>
        private string constellations = "獅子座";

        /// <summary>
        /// ーザの地域情報は、「場所リスト」に含まれるもののいずれかを指定
        /// </summary>
        private string place = "名古屋";

        /// <summary>
        /// 対話のモードは、下記のいずれかを指定
        /// dialog (省略時)
        /// srtr
        /// ※会話(しりとり)を継続する場合は、レスポンスボディのmodeの値を指定する
        /// </summary>
        private string mode = "dialog";

        ///// <summary>
        ///// キャラクタは、下記のいずれかを指定
        ///// 20 : 関西弁キャラ
        ///// 30 : 赤ちゃんキャラ
        ///// 指定なし : デフォルトキャラ
        ///// </summary>
        //private int t = 0;
        #endregion

        #region property
        public string Utt
        {
            get
            {
                return utt;
            }
            set
            {
                if (value.Length > 255)
                {
                    // too long
                    utt = "";
                }
                else
                {
                    utt = value;
                }
            }
        }

        public string Context
        {
            get
            {
                return context;
            }
            set
            {
                if (value.Length > 255)
                {
                    // too long
                    context = "";
                }
                else
                {
                    context = value;
                }
            }
        }

        public string Nickname
        {
            get
            {
                return nickname;
            }
            set
            {
                if (value.Length > 10)
                {
                    // too long
                    nickname = "";
                }
                else
                {
                    nickname = value;
                }
            }
        }

        public string Nickname_y
        {
            get
            {
                return nickname_y;
            }
            set
            {
                if (value.Length > 20)
                {
                    // too long
                    nickname_y = "";
                }
                else
                {
                    nickname_y = value;
                }
            }
        }

        public string Sex
        {
            get
            {
                return sex;
            }
            set
            {
                if (value != "男" && value != "女")
                {
                    Debug.Log("invaild value!");
                    sex = "";
                }
                else
                {
                    sex = value;
                }
            }
        }

        public string Bloodtype
        {
            get
            {
                return bloodtype;
            }
            set
            {
                if (value.Length > 1)
                {
                    Debug.Log(value.Length);
                    // too long
                    bloodtype = "";
                }
                else
                {
                    bloodtype = value;
                }
            }
        }

        public int BirthdateY
        {
            get
            {
                return birthdateY;
            }
            set
            {
                if (0 < value && value <= DateTime.Now.Year)
                {
                    birthdateY = value;
                }
                else
                {
                    birthdateY = 0;
                }
            }
        }

        public int BirthdateM
        {
            get
            {
                return birthdateM;
            }
            set
            {
                if (0 < value && value < 13)
                {
                    birthdateM = value;
                }
                else
                {
                    birthdateM = 0;
                }
            }
        }

        public int BirthdateD
        {
            get
            {
                return birthdateD;
            }
            set
            {
                if (0 < value && value < 31)
                {
                    birthdateD = value;
                }
                else
                {
                    birthdateD = 0;
                }
            }
        }

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (0 < value)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                }
            }
        }

        public string Constellations
        {
            get
            {
                return constellations;
            }
            set
            {
                if (value.Length > 3)
                {
                    // too long
                    constellations = "";
                }
                else
                {
                    constellations = value;
                }
            }
        }

        public string Place
        {
            get
            {
                return place;
            }
            set
            {
                if (value.Length > 10)
                {
                    // too long
                    place = "";
                }
                else
                {
                    place = value;
                }
            }
        }

        public string Mode
        {
            get
            {
                return mode;
            }
            set
            {
                if (value != "dialog" && value != "srtr")
                {
                    mode = "";
                }
                else
                {
                    mode = value;
                }
            }
        }

        //public int T
        //{
        //    get
        //    {
        //        return t;
        //    }
        //    set
        //    {
        //        t = value;
        //    }
        //}
        #endregion

        #region method
        // method to export json
        public string ExportJson()
        {
            //		get value from propertyInfo[]
            //		Debug.Log (user.GetType().GetProperties()[2].GetValue(user,null));
            //		get name from propertyInfo[]
            //		Debug.Log (user.GetType().GetProperties()[3].Name);
            PropertyInfo[] UserInfoArray = GetType().GetProperties();
            //		adding member strings to returnJson
            string returnJson = "{\n";
            foreach (PropertyInfo uInfoMember in UserInfoArray)
            {

                if (uInfoMember.PropertyType == typeof(int) && uInfoMember.GetValue(this, null).Equals(0))
                {
                    continue;
                }
                else if (uInfoMember.Name == "Mode")
                {
                    returnJson += "\"" + uInfoMember.Name.ToLower() + "\": \"" + uInfoMember.GetValue(this, null) + "\"\n";
                }
                else
                {
                    returnJson += "\"" + uInfoMember.Name.ToLower() + "\": \"" + uInfoMember.GetValue(this, null) + "\",\n";
                }
            }
            returnJson += "}";
            return returnJson;
        }
        #endregion
    }
}