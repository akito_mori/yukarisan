﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;


namespace HC
{
    [DisallowMultipleComponent]
    sealed public class MMD4EyeJitter : MonoBehaviourBase
    {
        #region variable
        [SerializeField]
        private float m_minInterval = 0.2f;
        [SerializeField]
        private float m_maxInterval = 0.5f;
        [SerializeField]
        private Vector2 m_range = new Vector2(7f, 3f);
        [SerializeField]
        private Vector2 m_microRange = new Vector2(0.5f, 0f);

        private float m_count = 0f;
        private Vector3 m_target = Vector3.zero;
        private bool m_amplitudeFlag = false;

        [SerializeField]
        private MMD4MecanimBone _leftEye = null;
        [SerializeField]
        private MMD4MecanimBone _rightEye = null;
        #endregion

        #region property
        private Vector3 Amplitude
        {
            get { return new Vector3(m_microRange.y, m_microRange.x, 0f); }
        }
        #endregion

        #region event
        private void Start()
        {
            this.UpdateAsObservable()
                .Do(_ => m_count -= Time.deltaTime)
                .Subscribe(_ =>
                {
                    if (m_count < 0f)
                    {
                        m_count = Random.Range(m_minInterval, m_maxInterval);
                        var x = Random.Range(-m_range.x, m_range.x);
                        var y = Random.Range(-m_range.y, m_range.y);
                        m_target = new Vector3(y, x, 0f);

                        SetRotation(m_target);
                    }

                    m_amplitudeFlag = !m_amplitudeFlag;
                    if (m_amplitudeFlag)
                    {
                        SetRotation(m_target);
                    }
                    else
                    {
                        SetRotation(m_target + Amplitude);
                    }
                });
        }
        #endregion

        #region method
        private void SetRotation(Vector3 target)
        {
            _leftEye.userRotation = Quaternion.Euler(target);
            _rightEye.userRotation = Quaternion.Euler(target);
        }
        #endregion
    }
}