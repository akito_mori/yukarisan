﻿using UnityEngine;
using WebSocketSharp;


namespace HC
{
    [DisallowMultipleComponent]
    sealed public class WSVoiceRecognition : MonoBehaviourBase
    {
        private WebSocket _ws;

        void Awake()
        {
            _ws = new WebSocket("ws://127.0.0.1:12002");
            _ws.OnMessage += (sender, e) =>
            {
                Debug.Log(e.Data); // 認識結果
            };
            _ws.Connect();
        }

        void OnApplicationQuit()
        {
            _ws.Close();
        }
    }
}