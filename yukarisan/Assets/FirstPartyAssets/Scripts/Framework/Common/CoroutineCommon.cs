﻿using UnityEngine;
using UniRx;
using System;
using System.Collections;


namespace HC
{
    /// <summary>
    /// コルーチンに関する汎用クラス
    /// </summary>
    public static class CoroutineCommon
    {
        #region method
        /// <summary>
        /// 1フレーム待機してからActionデリゲートを呼び出します
        /// </summary>
        public static void CallWaitForEndOfFrame(GameObject gameObject, Action act)
        {
            Observable
                .NextFrame()
                .Subscribe(_ => act())
                .AddTo(gameObject);
        }

        /// <summary>
        /// 指定された秒数待機してからActionデリゲートを呼び出します
        /// </summary>
        public static void CallWaitForSeconds(GameObject gameObject, float seconds, Action act)
        {
            Observable
                .Timer(TimeSpan.FromSeconds(seconds))
                .Subscribe(_ => act())
                .AddTo(gameObject);
        }

        /// <summary>
        /// 条件を満たしたらActionデリゲートを呼び出します
        /// </summary>
        public static void CallWaitForCondition(GameObject gameObject, Func<bool> condition, Action act)
        {
            if (condition())
            {
                act();
                return;
            }

            Observable
                .FromCoroutine<int>(observer => DoCallWaitForCondition(observer, condition))
                .Subscribe(_ => act())
                .AddTo(gameObject);
        }

        /// <summary>
        /// 条件を満たしたらActionデリゲートを呼び出します
        /// </summary>
        private static IEnumerator DoCallWaitForCondition(IObserver<int> observer, Func<bool> condition)
        {
            while (!condition()) yield return 0;
            observer.OnNext(0);
            observer.OnCompleted();
        }
        #endregion
    }
}