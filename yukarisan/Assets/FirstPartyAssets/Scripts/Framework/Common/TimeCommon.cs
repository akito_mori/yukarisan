﻿namespace HC
{
    public static class TimeCommon
    {
        #region enum
        public enum TimeUnit
        {
            Milliseconds,
            Seconds,
        }
        #endregion

        #region variable
        /// <summary>
        /// 秒からミリ秒に変換する係数
        /// </summary>
        public static readonly float SecondsToMilliseconds = 1000.0f;
        /// <summary>
        /// ミリ秒から秒に変換する係数
        /// </summary>
        public static readonly float MillisecondsToSeconds = 0.001f;
        #endregion
    }
}