﻿using UnityEngine;


namespace HC
{
    public static class Vector3Common
    {
        #region variable
        private static Vector3 s_vector3 = Vector3.zero;
        #endregion

        #region method
        #region Sum
        public static Vector3 SumX(Vector3 vector3, float x)
        {
            s_vector3.Set(vector3.x + x, vector3.y, vector3.z);
            return s_vector3;
        }

        public static Vector3 SumY(Vector3 vector3, float y)
        {
            s_vector3.Set(vector3.x, vector3.y + y, vector3.z);
            return s_vector3;
        }

        public static Vector3 SumZ(Vector3 vector3, float z)
        {
            s_vector3.Set(vector3.x, vector3.y, vector3.z + z);
            return s_vector3;
        }
        #endregion
        /// <summary>
        /// 最小値から最大値の範囲のランダムなベクトルを返す
        /// </summary>
        /// <param name="min">最小値</param>
        /// <param name="max">最大値</param>
        /// <returns>min ≦ value ≦ max</returns>
        public static Vector3 RandomRange(Vector3 min, Vector3 max)
        {
            s_vector3.Set(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
            return s_vector3;
        }

        /// <summary>
        /// 各成分が1か-1のベクトルを返す
        /// </summary>
        /// <returns>各成分が1か-1のベクトル</returns>
        public static Vector3 RandomSign()
        {
            s_vector3.Set(RandomCommon.Sign(), RandomCommon.Sign(), RandomCommon.Sign());
            return s_vector3;
        }

        /// <summary>
        /// 指定のオブジェクトが現在のオブジェクトと等しいかどうかを判断する
        /// </summary>
        /// <param name="self">自身</param>
        /// <param name="obj">比較対象</param>
        /// <param name="threshold">閾値</param>
        /// <returns></returns>
        public static bool SafeEquals(this Vector3 self, Vector3 obj, float threshold = 0.001f)
        {
            return self.x.SafeEquals(obj.x, threshold) && self.y.SafeEquals(obj.y, threshold) && self.z.SafeEquals(obj.z, threshold);
        }
        #endregion
    }
}