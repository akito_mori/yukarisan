﻿using UnityEngine;
using UnityEngine.AI;
using System;


namespace HC
{
    public static class NavMeshAgentCommon
    {
        #region method
        /// <summary>
        /// 目的地を試行する(失敗した場合はNavMeshの設計に問題があるのでNavMeshを確認すること)
        /// </summary>
        /// <param name="destinationCalculation">目的地を設定する計算式</param>
        /// <param name="trialCount">試行回数</param>
        /// <param name="maxDistance">サンプリングされるsourcePositionからの距離</param>
        /// <returns>目的地</returns>
        public static Vector3? TrialDestination(Func<Vector3> destinationCalculation, int trialCount = 1, float maxDistance = 0.1f)
        {
            for (int i = 0; i < trialCount; ++i)
            {
                // 目的地を決める
                Vector3 destination = destinationCalculation();

                NavMeshHit hit;
                // 目的地の座標がNavMeshの座標範囲内の場合
                if (NavMesh.SamplePosition(destination, out hit, maxDistance, NavMesh.AllAreas))
                {
                    return (Vector3?)destination;
                }
            }

            Debug.LogWarning("目的地の試行に失敗しました、NavMeshの設計に問題があると思われます。");
            // 試行回数を超えた場合失敗する
            return null;
        }
        #endregion
    }
}