﻿using UnityEngine;


namespace HC
{
    public static class RandomCommon
    {
        #region method
        /// <summary>
        /// 最大値を含めた最小値から最大値の範囲の乱数を返す
        /// </summary>
        /// <param name="min">最小値</param>
        /// <param name="max">最大値</param>
        /// <returns>min ≦ value ≦ max</returns>
        public static int IncludeMaxRange(int min, int max)
        {
            return Random.Range(min, max + 1);
        }

        /// <summary>
        /// 1か-1を返す
        /// </summary>
        /// <returns>1か-1</returns>
        public static int Sign()
        {
            return IncludeMaxRange(0, 1) == 1 ? 1 : -1;
        }

        /// <summary>
        /// x/1.0fの確率でtrueを返す
        /// </summary>
        public static bool Percent(float x)
        {
            return Random.value <= x;
        }
        #endregion
    }
}