﻿using UnityEngine;


namespace HC
{
    public static class MathCommon
    {
        #region variable
        /// <summary>
        /// 最大角度
        /// </summary>
        public static readonly float MaxAngle = 360.0f;
        /// <summary>
        /// 最大角度の半分
        /// </summary>
        public static readonly float HalfAngle = 180.0f;
        /// <summary>
        /// 最小角度
        /// </summary>
        public static readonly float MinAngle = 0.0f;


        private static Vector3 s_vector3 = Vector3.zero;
        #endregion

        #region method
        /// <summary>
        /// ランダムな角度を取得する
        /// </summary>
        /// <returns>ランダムな角度</returns>
        public static float RandomAngle()
        {
            return Random.Range(MinAngle, MaxAngle);
        }

        /// <summary>
        /// ランダムな角度を取得する
        /// </summary>
        /// <returns>ランダムな角度</returns>
        public static Vector3 RandomAngles()
        {
            s_vector3.Set(RandomAngle(), RandomAngle(), RandomAngle());
            return s_vector3;
        }

        /// <summary>
        /// ランダムなXZ平面のベクトルを取得する
        /// </summary>
        /// <returns>ランダムなXZ平面のベクトル</returns>
        public static Vector3 RandomVectorXZ()
        {
            Vector3 randomVectorXZ = Quaternion.Euler(MinAngle, RandomAngle(), MinAngle) * Vector3.forward;
            s_vector3.Set(randomVectorXZ.x, 0.0f, randomVectorXZ.z);
            return s_vector3;
        }

        /// <summary>
        /// 回転方向の左右判定を行う
        /// </summary>
        /// <param name="nowAngle">現時点での角度</param>
        /// <param name="targetAngle">最終的な角度</param>
        /// <returns>true: 時計回り    false: 反時計周り</returns>
        public static bool CheckClockwise(float nowAngle, float targetAngle)
        {
            return targetAngle > nowAngle ? !(targetAngle - nowAngle > HalfAngle) : nowAngle - targetAngle > HalfAngle;
        }

        /// <summary>
        /// 桁数の計算
        /// </summary>
        /// <param name="num">計算する値</param>
        /// <returns></returns>
        public static int Digit(int num)
        {
            int count = 1;

            while ((num /= 10) != 0)
            {
                ++count;
            }
            return count;
        }
        #endregion
    }
}