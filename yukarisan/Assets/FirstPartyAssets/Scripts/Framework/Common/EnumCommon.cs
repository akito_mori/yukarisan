﻿using System;
using System.Linq;


namespace HC
{
    public static class EnumCommon
    {
        #region variable
        private static readonly Random RandomEngine = new Random();
        #endregion

        #region method
        /// <summary>
        /// 指定された列挙型の値をランダムに取得する
        /// </summary>
        public static T Random<T>()
        {
            return Enum.GetValues(typeof(T))
                .Cast<T>()
                .OrderBy(c => RandomEngine.Next())
                .FirstOrDefault();
        }

        /// <summary>
        /// 指定された列挙型の要素の数を取得する
        /// </summary>
        public static int GetLength<T>()
        {
            return Enum.GetValues(typeof(T)).Length;
        }
        #endregion
    }
}