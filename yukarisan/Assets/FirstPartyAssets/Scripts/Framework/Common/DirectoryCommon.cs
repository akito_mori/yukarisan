﻿using System.IO;


namespace HC
{
    public static class DirectoryCommon
    {
        /// <summary>
        /// 指定したパスにディレクトリが存在しない場合全てのディレクトリとサブディレクトリを作成する
        /// </summary>
        public static DirectoryInfo SafeCreateDirectory(string path)
        {
            if (Directory.Exists(path))
                return null;

            return Directory.CreateDirectory(path);
        }
    }
}