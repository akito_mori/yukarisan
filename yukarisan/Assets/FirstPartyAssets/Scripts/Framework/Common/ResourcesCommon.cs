﻿using UnityEngine;


namespace HC
{
    public static class ResourcesCommon
    {
        #region method
        /// <summary>
        /// リソースを読み込みます
        /// </summary>
        /// <typeparam name="T">リソースの型</typeparam>
        /// <param name="path">リソースのファイルパス</param>
        /// <returns>読み込んだリソース</returns>
        public static T Load<T>(string path) where T : Object
        {
            return Resources.Load(path, typeof(T)) as T;
        }

        /// <summary>
        /// テクスチャを読み込みます
        /// </summary>
        /// <param name="path">テクスチャのファイルパス</param>
        /// <returns>読み込んだテクスチャ</returns>
        public static Texture LoadTexture(string path)
        {
            return Load<Texture>("Textures/" + path);
        }

        /// <summary>
        /// プレハブを読み込みます
        /// </summary>
        /// <param name="path">プレハブのファイルパス</param>
        /// <returns>読み込んだプレハブ</returns>
        public static GameObject LoadPrefab(string path)
        {
            return Load<GameObject>("Prefabs/" + path);
        }

        /// <summary>
        /// マテリアルを読み込みます
        /// </summary>
        /// <param name="path">マテリアルのファイルパス</param>
        /// <returns>読み込んだマテリアル</returns>
        public static Material LoadMaterial(string path)
        {
            return Load<Material>("Materials/" + path);
        }

        /// <summary>
        /// オーディオクリップを読み込みます
        /// </summary>
        /// <param name="path">オーディオクリップのファイルパス</param>
        /// <returns>読み込んだオーディオクリップ</returns>
        public static AudioClip LoadAudioClip(string path)
        {
            return Load<AudioClip>("Audio/" + path);
        }

        /// <summary>
        /// テキストアセットを読み込みます
        /// </summary>
        /// <param name="path">テキストアセットのファイルパス</param>
        /// <returns>読み込んだテキストアセット</returns>
        public static TextAsset LoadTextAsset(string path)
        {
            return Load<TextAsset>("TextAssets/" + path);
        }
        #endregion
    }
}