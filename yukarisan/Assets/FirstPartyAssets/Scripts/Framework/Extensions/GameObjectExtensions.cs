﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;


namespace HC
{
    public static class GameObjectExtensions
    {
        #region method
        /// <summary>
		/// nullチェックした上でGameObjectをDestory
		/// </summary>
		/// <param name="self">破棄するGameObject</param>
		public static void SafeDestroy(this GameObject self)
        {
            if (null != self)
            {
                GameObject.Destroy(self);
                self = null;
            }
        }

        /// <summary>
        /// 全ての子オブジェクトを取得する
        /// </summary>
        /// <param name="self">GameObject型のインスタンス</param>
        /// <param name="includeInactive">非アクティブなオブジェクトも取得する場合 true</param>
        /// <returns>全ての子オブジェクトを管理するリスト</returns>
        public static List<GameObject> GetAllChildren(this GameObject self, bool includeInactive = false)
        {
            return self.GetComponentsInChildren<Transform>(includeInactive)
                .Where(t => t != self.transform)
                .Select(t => t.gameObject)
                .ToList();
        }

        /// <summary>
        /// 子オブジェクトを取得する
        /// </summary>
        /// <param name="self">GameObject型のインスタンス</param>
        /// <param name="includeInactive">非アクティブなオブジェクトも取得する場合 true</param>
        /// <returns>子オブジェクトを管理するリスト</returns>
        public static List<GameObject> GetChildren(this GameObject self, bool includeInactive = false)
        {
            List<GameObject> children = new List<GameObject>();

            foreach (Transform child in self.transform)
            {
                if (includeInactive || child.gameObject.activeSelf)
                {
                    children.Add(child.gameObject);
                }
            }

            return children;
        }
        #endregion
    }
}