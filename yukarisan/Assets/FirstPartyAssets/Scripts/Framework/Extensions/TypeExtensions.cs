﻿using System;
using System.Linq;


namespace HC
{
    public static class TypeExtensions
    {
        #region method
        /// <summary>
        /// 型が指定されたいずれかの型と等しいかどうかを返します
        /// </summary>
        public static bool IsAny(this Type self, params Type[] values)
        {
            return values.Any(t => t == self);
        }
        #endregion
    }
}