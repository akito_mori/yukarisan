﻿using UnityEngine;


namespace HC
{
    public static class ArrayExtensions
    {
        #region method
        /// <summary>
        /// ランダムに配列内の要素を取得する
        /// </summary>
        /// <typeparam name="T">配列要素の型</typeparam>
        /// <param name="self">配列</param>
        /// <returns>配列内の要素</returns>
        public static T GetRandom<T>(this T[] self)
        {
            return self[Random.Range(0, self.Length)];
        }

        /// <summary>
        /// 順番に配列内の要素を取得する
        /// </summary>
        /// <typeparam name="T">配列要素の型</typeparam>
        /// <param name="self">配列</param>
        /// <param name="t">値</param>
        /// <returns>配列内の要素</returns>
        public static T GetOrder<T>(this T[] self, int t)
        {
            return self[t % self.Length];
        }

        /// <summary>
        /// 配列の先頭要素の取得
        /// </summary>
        /// <typeparam name="T">配列要素の型</typeparam>
        /// <param name="self">配列</param>
        /// <returns>配列の先頭要素</returns>
        public static T Begin<T>(this T[] self)
        {
            return self[self.BeginIndex()];
        }

        /// <summary>
        /// 配列の先頭要素の添字の取得
        /// </summary>
        /// <typeparam name="T">配列の要素型</typeparam>
        /// <param name="self">配列</param>
        /// <returns>配列の先頭要素の添字</returns>
        public static int BeginIndex<T>(this T[] self)
        {
            return 0;
        }
        #endregion
    }
}