﻿using System.IO;
using System.Linq;


namespace HC
{
    public static class StringExtensions
    {
        #region method
        /// <summary>
        /// 自身の値がnullまたは空か判定(string.IsNullOrEmpty)
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string self)
        {
            return string.IsNullOrEmpty(self);
        }

        /// <summary>
        /// 拡張子の取得(Path.GetExtension)
        /// </summary>
        /// <param name="self">自身</param>
        /// <returns></returns>
        public static string GetExtension(this string self)
        {
            return Path.GetExtension(self);
        }


        /// <summary>
        /// ファイルパスからファイル名を取得(Path.GetFileName)
        /// </summary>
        /// <param name="self">自身</param>
        /// <returns></returns>
        public static string GetFileName(this string self)
        {
            return Path.GetFileName(self);
        }


        /// <summary>
        /// ファイルパスから拡張子を除いたファイル名を取得(Path.GetFileNameWithoutExtensions)
        /// </summary>
        /// <param name="self">自身</param>
        /// <returns></returns>
        public static string GetFileNameWithoutExtensions(this string self)
        {
            return Path.GetFileNameWithoutExtension(self);
        }


        /// <summary>
        /// 指定した文字列を末尾から削除
        /// </summary>
        /// <param name="self">自身</param>
        /// <param name="value">削除したい文字列</param>
        /// <returns></returns>
        public static string RemoveAtLast(this string self, string value)
        {
            return self.Remove(self.LastIndexOf(value), value.Length);
        }

        /// <summary>
        /// 指定した文字列をすべて空文字列に置換した新しい文字列を返します
        /// </summary>
        /// <remarks>
        /// "ABCABC".ReplaceEmpty("B") → ACAC
        /// </remarks>
        public static string ReplaceEmpty(this string self, string oldValue)
        {
            return self.Replace(oldValue, string.Empty);
        }

        /// <summary>
        /// 文字列が指定されたいずれかの文字列と等しいかどうかを返します
        /// </summary>
        public static bool IsAny(this string self, params string[] values)
        {
            return values.Any(c => c == self);
        }
        #endregion
    }
}