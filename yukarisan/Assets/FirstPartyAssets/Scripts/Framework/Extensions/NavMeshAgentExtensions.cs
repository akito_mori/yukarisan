﻿using UnityEngine;
using UnityEngine.AI;


namespace HC
{
    public static class NavMeshAgentExtensions
    {
        #region method
        /// <summary>
        /// NavMeshAgentを直ちに停止する(NavMeshAgent.Stop()を使用すると停止するまでに慣性が掛かる)
        /// </summary>
        /// <param name="self">自身</param>
        public static void Pause(this NavMeshAgent self)
        {
            self.velocity = Vector3.zero;
            self.Stop();
        }
        #endregion
    }
}