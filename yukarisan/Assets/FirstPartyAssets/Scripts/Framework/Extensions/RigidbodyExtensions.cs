﻿using UnityEngine;


namespace HC
{
    public static class RigidbodyExtensions
    {
        #region method
        /// <summary>
        /// 物理演算を初期化する
        /// </summary>
        /// <param name="self">自身</param>
        public static void Initialize(this Rigidbody self)
        {
            self.velocity = Vector3.zero;
            self.angularVelocity = Vector3.zero;
        }
        #endregion
    }
}