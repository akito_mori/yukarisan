﻿using System.Collections.Generic;


namespace HC
{
    public static class ListExtensions
    {
        #region method
        /// <summary>
        /// Listの先頭要素の取得
        /// </summary>
        /// <typeparam name="Type">Listの要素型</typeparam>
        /// <param name="self">List自身</param>
        /// <returns>Listの先頭要素</returns>
        public static Type Begin<Type>(this List<Type> self)
        {
            return self[0];
        }

        /// <summary>
        /// Listの末尾要素の取得
        /// </summary>
        /// <typeparam name="Type">Listの要素型</typeparam>
        /// <param name="self">List自身</param>
        /// <returns>Listの末尾要素</returns>
        public static Type Back<Type>(this List<Type> self)
        {
            return self[self.BackIndex()];
        }

        /// <summary>
        /// Listの末尾要素の添字の取得
        /// </summary>
        /// <typeparam name="Type">Listの要素型</typeparam>
        /// <param name="self">List自身</param>
        /// <returns>Listの末尾要素の添字</returns>
        public static int BackIndex<Type>(this List<Type> self)
        {
            return self.Count - 1;
        }

        /// <summary>
        /// Listの先頭要素の削除
        /// </summary>
        /// <typeparam name="Type">Listの要素型</typeparam>
        /// <param name="self">List自身</param>
        public static void RemoveBegin<Type>(this List<Type> self)
        {
            self.RemoveAt(0);
        }

        /// <summary>
        /// Listの末尾要素の削除
        /// </summary>
        /// <typeparam name="Type">Listの要素型</typeparam>
        /// <param name="self">List自身</param>
        public static void RemoveBack<Type>(this List<Type> self)
        {
            self.RemoveAt(self.BackIndex());
        }


        /// <summary>
        /// Listの要素にアクセス可能か判断
        /// </summary>
        /// <typeparam name="Type">Listの要素型</typeparam>
        /// <param name="self">List自身</param>
        /// <param name="e">アクセスしたい要素</param>
        /// <returns>true:アクセス可(要素が存在する), false:アクセス不可</returns>
        public static bool ElementExist<Type>(this List<Type> self, int e)
        {
            return self.Count > e;
        }
        #endregion
    }
}