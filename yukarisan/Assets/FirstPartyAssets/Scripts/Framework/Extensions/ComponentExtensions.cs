﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;


namespace HC
{
    public static class ComponentExtensions
    {
        #region SafeDestroy

        /// <summary>
		/// nullチェックした上でComponentをDestory
		/// </summary>
		/// <param name="Component">破棄するComponent</param>
		public static void SafeDestroy(this Component self)
        {
            if (null != self)
            {
                GameObject.Destroy(self);
                self = null;
            }
        }

        #endregion

        #region GetAllChildren

        /// <summary>
        /// 全ての子オブジェクトを取得する
        /// </summary>
        /// <param name="self">Component型のインスタンス</param>
        /// <param name="includeInactive">非アクティブなオブジェクトも取得する場合 true</param>
        /// <returns>全ての子オブジェクトを管理するリスト</returns>
        public static List<GameObject> GetAllChildren(this Component self, bool includeInactive = false)
        {
            return self.GetComponentsInChildren<Transform>(includeInactive)
                .Where(t => t != self.transform)
                .Select(t => t.gameObject)
                .ToList();
        }

        #endregion

        #region GetChildren

        /// <summary>
        /// 子オブジェクトを取得する
        /// </summary>
        /// <param name="self">Component型のインスタンス</param>
        /// <param name="includeInactive">非アクティブなオブジェクトも取得する場合 true</param>
        /// <returns>子オブジェクトを管理するリスト</returns>
        public static List<GameObject> GetChildren(this Component self, bool includeInactive = false)
        {
            List<GameObject> children = new List<GameObject>();

            foreach (Transform child in self.transform)
            {
                if (includeInactive || child.gameObject.activeSelf)
                {
                    children.Add(child.gameObject);
                }
            }

            return children;
        }

        #endregion

        #region GetOrAddComponent

        /// <summary>
        /// Componentの取得、GameObjectにComponentがない場合はAddして取得する
        /// </summary>
        public static T GetOrAddComponent<T>(this Component c) where T : Component
        {
            if (c.gameObject == null)
            {
                return default(T);
            }

            return c.gameObject.GetOrAddComponent<T>();
        }

        /// <summary>
        /// Componentの取得、GameObjectにComponentがない場合はAddして取得する
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject go) where T : Component
        {
            var component = go.GetComponent<T>();
            if (component == null)
            {
                component = go.AddComponent<T>();
            }

            return component;
        }

        #endregion

        #region GetComponentInChildren

        public static T GetComponentInChildren<T>(this Component component, string name) where T : Component
        {
            return component.transform.FindInChildren(name).GetComponent<T>();
        }

        #endregion

        #region GetComponentInFamily
        /*
         * transform.GetComponentInFamily<Component>(2);
         * の場合
         * 
         * ┬A...
         * ├┬B
         * │├─C(自身)
         * ...
         * Cから2段階昇ったオブジェクト以下からコンポーネントを探す
         */

        #region hierarchy
        /// <summary>
        /// 遡った親以下のオブジェクトからコンポーネントを取得
        /// </summary>
        /// <param name="hierarchy">遡る親の階層</param>
        public static T GetComponentInFamily<T>(this Component component, int hierarchy) where T : Component
        {
            Transform parent = component.transform;

            for (int i = 0; i < hierarchy; ++i)
            {
                parent = parent.parent;
            }

            return parent.GetComponentInChildren<T>();
        }

        /// <summary>
        /// 遡った親以下のオブジェクトからコンポーネントを複数取得
        /// </summary>
        /// <param name="hierarchy">遡る親の階層</param>
        public static T[] GetComponentsInFamily<T>(this Component component, int hierarchy) where T : Component
        {
            Transform parent = component.transform;

            for (int i = 0; i < hierarchy; ++i)
            {
                parent = parent.parent;
            }

            return parent.GetComponentsInChildren<T>();
        }

        #endregion

        #region auto safe

        /// <summary>
        /// 遡った親以下のオブジェクトからコンポーネントを取得(一番近い階層から取得)
        /// 一定以上遡ってもComponentを見つけられない場合諦める
        /// </summary>
        public static T GetComponentInFamily<T>(this Component component) where T : Component
        {
            Transform parent = component.transform;

            int count = 0;

            while (parent.GetComponentInChildren<T>() == null)
            {
                parent = parent.parent;

                ++count;

                if (count > 1000)
                {
                    Debug.LogWarning("GetComponentInFamilySafe : component not found");
                    return null;
                }
            }

            return parent.GetComponentInChildren<T>();
        }

        /// <summary>
        /// 遡った親以下のオブジェクトからコンポーネントを複数取得(一番近い階層から取得)
        /// 一定以上遡ってもComponentを見つけられない場合諦める
        /// </summary>
        public static T[] GetComponentsInFamily<T>(this Component component) where T : Component
        {
            Transform parent = component.transform;

            int count = 0;

            while (parent.GetComponentInChildren<T>() == null)
            {
                parent = parent.parent;

                ++count;

                if (count > 1000)
                {
                    Debug.LogWarning("GetComponentInFamilySafe : components not found");
                    return null;
                }
            }

            return parent.GetComponentsInChildren<T>();
        }
        #endregion

        #endregion

        #region FindInFamily

        /// <summary>
        /// 遡った親の子から指定した名前のtransformを取得
        /// </summary>
        /// <param name="hierarchy">遡る親の階層</param>
        public static Transform FindInFamily(this Component component, string name, int hierarchy)
        {
            Transform parent = component.transform;

            for (int i = 0; i < hierarchy; ++i)
            {
                parent = parent.parent;
            }

            return parent.FindInChildren(name);
        }

        /// <summary>
        /// 遡った親の子から指定した名前のtransformを取得(一番近い階層から取得)
        /// 一定以上遡ってもtransformを見つけられない場合諦める
        /// </summary>
        public static Transform FindInFamily(this Component component, string name)
        {
            Transform parent = component.transform;

            int count = 0;

            while (parent.FindInChildren(name) == null)
            {
                parent = parent.parent;

                ++count;

                if (count > 1000)
                {
                    Debug.LogWarning("FindInFamily :" + name + "gameobject not found");
                    return null;
                }
            }

            return parent.FindInChildren(name);
        }

        #endregion
    }

    /// <summary>
    /// 用法を誤ると無限ループを引き起こす等扱いに注意が必要なもの
    /// </summary>
    namespace Unsafe
    {
        public static class ComponentExtensions
        {
            #region GetComponentInFamily
            /// <summary>
            /// 遡った親以下のオブジェクトからコンポーネントを取得(一番近い階層から取得)
            /// 遡るオブジェクトに限界はないが無限ループの可能性があるためGetComponentInFamilySafeを基本的に使用する
            /// </summary>
            public static T GetComponentInFamilyUnsafe<T>(this Component component) where T : Component
            {
                Transform parent = component.transform;

                while (parent.GetComponentInChildren<T>() == null)
                {
                    parent = parent.parent;
                }

                return parent.GetComponentInChildren<T>();
            }

            /// <summary>
            /// 遡った親以下のオブジェクトからコンポーネントを複数取得(一番近い階層から取得)
            /// 遡るオブジェクトに限界はないが無限ループの可能性があるためGetComponentInFamilySafeを基本的に使用する
            /// </summary>
            public static T[] GetComponentsInFamilyUnsafe<T>(this Component component) where T : Component
            {
                Transform parent = component.transform;

                while (parent.GetComponentInChildren<T>() == null)
                {
                    parent = parent.parent;
                }

                return parent.GetComponentsInChildren<T>();
            }

            #endregion
        }
    }
}