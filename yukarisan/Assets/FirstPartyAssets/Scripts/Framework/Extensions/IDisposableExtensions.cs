﻿using System;


namespace HC
{
    public static class IDisposableExtensions
    {
        #region method
        /// <summary>
        /// nullチェックした上でIDisposableをDispose
        /// </summary>
        public static void SafeDispose(this IDisposable self)
        {
            if (null != self)
            {
                self.Dispose();
                self = null;
            }
        }
        #endregion
    }
}