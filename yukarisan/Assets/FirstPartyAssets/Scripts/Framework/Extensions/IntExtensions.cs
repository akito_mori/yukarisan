﻿using System.Linq;


namespace HC
{
    public static class IntExtensions
    {
        #region method
        /// <summary>
        /// 偶数かどうかを返します
        /// </summary>
        public static bool IsEven(this int self)
        {
            return self % 2 == 0;
        }

        /// <summary>
        /// 奇数かどうかを返します
        /// </summary>
        public static bool IsOdd(this int self)
        {
            return self % 2 == 1;
        }

        /// <summary>
        /// 値が指定されたいずれかの値と等しいかどうかを返します
        /// </summary>
        public static bool IsAny(this int self, params int[] values)
        {
            return values.Any(c => c == self);
        }
        #endregion
    }
}