﻿using UnityEngine;


namespace HC
{
    public static class MathExtensions
    {
        #region method
        #region float
        /// <summary>
        /// 指定のオブジェクトが現在のオブジェクトと等しいかどうかを判断する
        /// </summary>
        /// <param name="self">自身</param>
        /// <param name="obj">比較対象</param>
        /// <param name="threshold">閾値</param>
        /// <returns></returns>
        public static bool SafeEquals(this float self, float obj, float threshold = 0.001f)
        {
            return Mathf.Abs(self - obj) <= threshold;
        }
        #endregion//!float

        #region Vector3
        /// <summary>
        /// 自身を中心とした円と点との判定
        /// (yは考慮しない)
        /// </summary>
        /// <param name="self">自身、円の中心</param>
        /// <param name="radius">円の半径</param>
        /// <param name="other">点</param>
        /// <returns>円に点が入っているか</returns>
        public static bool IsInRangeCirc(this Vector3 self, float radius, Vector3 other)
        {
            //  (other.x - self.x)^2 +(other.z - self.z)^2 <= radius^2
            return ((Mathf.Pow((other.x - self.x), 2f)) + (Mathf.Pow((other.z - self.z), 2f))) <= (Mathf.Pow(radius, 2f));
        }


        /// <summary>
        /// 自身を中心とした球と点の判定
        /// </summary>
        /// <param name="self">自身の中心座標</param>
        /// <param name="radius">球の半径</param>
        /// <param name="other">点</param>
        /// <returns>球に点が入っているか</returns>
        public static bool IsInRangeSphere(this Vector3 self, float radius, Vector3 other)
        {
            //  (other.x - self.x)^2 + (other.y - self.y)^2 + (other.z - self.z)^2 <= radius^2
            return (Mathf.Pow(other.x - self.x, 2f)) + (Mathf.Pow(other.y - self.y, 2f)) + (Mathf.Pow(other.z - self.z, 2f)) <= (Mathf.Pow(radius, 2f));
        }
        #endregion//!Vector3
        #endregion
    }
}