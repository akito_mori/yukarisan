﻿using UnityEngine;


namespace HC
{
    public abstract class MonoBehaviourBase : MonoBehaviour
    {
        #region variable
        private Transform _transform = null;
        #region property
        public Transform t
        {
            get { return _transform ?? (_transform = transform); }
        }
        #endregion
        #endregion
    }
}